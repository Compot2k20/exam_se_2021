import java.util.ArrayList;

public class subject1 {
    class Vehicle {
    }

    class Car extends Vehicle {
        private String color;

        public void start() {
        }

        public void stop() {
        }

        public void go() {
        }
    }

    class Windshield {
        public Car c;

        Windshield() {
            this.c = new Car();
        }
    }

    class Wheel {
        public ArrayList<Car> c = new ArrayList<>();

        Wheel() {
            for(int i = 0; i < 4; i++) {
                c.add(new Car());
            }
        }
    }

    class CarMaker {
        private Car c;
    }

    class User {

    }
}
