import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class subject2 implements ActionListener {
    JFrame panel = new JFrame();
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();
    JButton button1 = new JButton();
    String str1 = "Hello World!";

    public subject2(){

        textField1.setBounds(50, 50, 100, 40);
        textField2.setBounds(250, 50, 100, 40);

        button1.setBounds(150, 200, 100, 25);
        button1.addActionListener(this);

        panel.add(textField1);
        panel.add(textField2);
        panel.add(button1);
        panel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel.setSize(400, 400);
        panel.setLayout(null);
        panel.setVisible(true);

    }

    public static void main(String[] args) {
        subject2 tSwitch = new subject2();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==button1) {
            String val1 = textField1.getText();
            if(val1.length()==0)
            {
                textField1.setText(str1);
                textField2.setText("");
            }
            else {
                textField2.setText(str1);
                textField1.setText("");
            }
        }
    }
}
